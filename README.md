Barcode explorer
================

Uses [BWIPP](https://github.com/bwipp/postscriptbarcode/),
[treepoem](https://github.com/adamchainz/treepoem), and
[Flask](http://flask.pocoo.org) to make an interactive UI
for generating barcodes.

Run with Flask test server
--------------------------

```python
$ PYTHONPATH=. python3 -m web
 * Running on http://0.0.0.0:8080/ (Press CTRL+C to quit)
 * Restarting with inotify reloader
 * Debugger is active!
 * Debugger PIN: 194-898-713
```

![screenshot](screenshot.png)

TODO
----

* [make `treepoem` not spin up 2 subprocesses for each barcode generated](https://github.com/adamchainz/treepoem/issues/145)
