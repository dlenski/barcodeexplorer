# -*- coding: utf-8 -*-
from flask import send_file, jsonify, request, Response, redirect, url_for, render_template
from os.path import splitext
from io import BytesIO

from treepoem import generate_barcode, barcode_types
from qrcode import QRCode, constants
from qrcode.image.svg import SvgPathImage

from web.fancy_barcode_types import fancy_barcode_types
from web.flaskapp import app

@app.route('/barcode', methods=['GET','POST'])
def full():
    opts = {**request.args.to_dict(), **request.form.to_dict()}
    code = opts.pop('code', None)
    format = opts.pop('format', 'qrcode')
    ext = opts.pop('ext', 'png')
    if code is None:
        return jsonify({'error':True, 'msg':"Form field 'code', must be specified."}), 500
    return barcode(code, format, ext, opts)

@app.route('/barcode/<format_and_filetype>')
def short1(format_and_filetype):
    opts = request.args.to_dict()
    format, filetype = splitext(format_and_filetype)
    ext = filetype[1:]
    code = opts.pop('code', None)
    if code is None:
        return jsonify({'error':True, 'msg':"Form field 'code' must be specified."}), 500
    if not ext:
        return redirect(url_for('short1', code=code, format_and_filetype=format + '.png', **opts))
    return barcode(code, format, ext, opts)

@app.route('/barcode/<format>/<path:code_and_filetype>')
def short2(format, code_and_filetype):
    opts = request.args
    code, filetype = splitext(code_and_filetype)
    ext = filetype[1:]
    if not ext:
        return redirect(url_for('barcode_get', format=format, code_and_filetype=code + '.png', **opts))
    return barcode(code, format, ext, opts)

def barcode(code, format, ext, opts):
    ext2mt = {'eps':'image/postscript', 'jpg':'image/jpeg', 'svg':'image/svg+xml', **{v:'image/'+v for v in ('png','bmp','jpeg','gif')}}
    if ext not in ext2mt:
        return jsonify({'error':True, 'value':filetype, 'msg':'Unsupported file extension.', 'allowed_values':list(ext2mt)}), 500
    if format not in barcode_types:
        return jsonify({'error':True, 'value':format, 'msg':'Unsupported barcode type.', 'allowed_values':list(barcode_types)}), 500
    if ext == 'svg' and format != 'qrcode':
        return jsonify({'error':True, 'msg':'File extension svg is supported ONLY for qrcode format'}), 500

    with BytesIO() as b:
        try:
            if ext == 'svg' and format == 'qrcode':
                args = {}
                if 'version' in opts: args['version'] = opts['version']
                if 'eclevel' in opts: args['error_correction'] = getattr(constants, 'ERROR_CORRECT_'+opts.get('eclevel'))
                qr = QRCode(image_factory=SvgPathImage, **args)
                qr.add_data(code)
                qr.make(fit='version' not in args)
                qr.make_image().save(b)
            else:
                im = generate_barcode(format, code, {k:v or True for k,v in opts.items()})
                if ext in ('eps',):
                    im.save(b, ext)
                else:
                    im.convert('1').save(b, ext)
            r = Response(b.getvalue(), mimetype=ext2mt[ext])
            r.cache_control.max_age = 31536000
            return r
        except Exception as e:
            return jsonify({'error':True, 'msg':str(e)}), 500

@app.route('/wiki')
@app.route('/wiki/<format>')
def wiki(format=None):
    for gn, g in fancy_barcode_types.items():
        for f, (fn, wiki) in g.items():
            if f == format:
                return redirect('//github.com/bwipp/postscriptbarcode/wiki/' + wiki)
    return jsonify({'error':True, 'msg':'Unsupported barcode type.', 'allowed_values':list(barcode_types)}), 500

@app.route('/')
def index():
    return render_template('index.html', fancy_barcode_types=fancy_barcode_types, default_barcode_type='qrcode')

if __name__ == '__main__':
    app.run(app.config['IP'], app.config['PORT'])
