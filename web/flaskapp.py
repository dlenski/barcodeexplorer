from flask import Flask

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')

app.debug = app.config['DEBUG']
