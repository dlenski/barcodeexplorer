# Autogenerated by digest_bwipp_wiki_symbologies.py, from the
# symbologies sidebar of the BWIPP wiki:
#    https://github.com/bwipp/postscriptbarcode.wiki.git)

fancy_barcode_types = {
    'Point of Sale': {
        'ean13': ('EAN-13', 'EAN-13'),
        'ean8': ('EAN-8', 'EAN-8'),
        'upca': ('UPC-A', 'UPC-A'),
        'upce': ('UPC-E', 'UPC-E'),
        'isbn': ('ISBN', 'ISBN'),
        'ismn': ('ISMN', 'ISMN'),
        'issn': ('ISSN', 'ISSN'),
    },
    'GS1 DataBar': {
        'databaromni': ('GS1 DataBar Omnidirectional', 'GS1-DataBar-Omnidirectional'),
        'databarstackedomni': ('GS1 DataBar Stacked Omnidirectional', 'GS1-DataBar-Stacked-Omnidirectional'),
        'databarexpanded': ('GS1 DataBar Expanded', 'GS1-DataBar-Expanded'),
        'databarexpandedstacked': ('GS1 DataBar Expanded Stacked', 'GS1-DataBar-Expanded-Stacked'),
        'databartruncated': ('GS1 DataBar Truncated', 'GS1-DataBar-Truncated'),
        'databarstacked': ('GS1 DataBar Stacked', 'GS1-DataBar-Stacked'),
        'databarlimited': ('GS1 DataBar Limited', 'GS1-DataBar-Limited'),
        'gs1northamericancoupon': ('GS1 North American Coupon Code', 'GS1-North-American-Coupon-Code'),
    },
    'Supply Chain': {
        'gs1datamatrix': ('GS1 DataMatrix', 'GS1-DataMatrix'),
        'gs1qrcode': ('GS1 QR Code', 'GS1-QR-Code'),
        'gs1-128': ('GS1-128', 'GS1-128'),
        'ean14': ('EAN-14', 'EAN-14'),
        'itf14': ('ITF-14', 'ITF-14'),
        'sscc18': ('SSCC-18', 'SSCC-18'),
    },
    'Two-dimensional Symbols': {
        'azteccode': ('Aztec Code', 'Aztec-Code'),
        'aztecrune': ('Aztec Runes', 'Aztec-Runes'),
        'datamatrix': ('Data Matrix', 'Data-Matrix'),
        'micropdf417': ('MicroPDF417', 'MicroPDF417'),
        'pdf417': ('PDF417', 'PDF417'),
        'pdf417compact': ('Compact PDF417', 'Compact-PDF417'),
        'qrcode': ('QR Code', 'QR-Code'),
        'microqrcode': ('Micro QR Code', 'Micro-QR-Code'),
        'hanxin': ('Han Xin Code', 'Han-Xin-Code'),
    },
    'One-dimensional Symbols': {
        'code128': ('Code 128', 'Code-128'),
        'code39': ('Code 39', 'Code-39'),
        'code39ext': ('Code 39 Extended', 'Code-39-Extended'),
        'code93': ('Code 93', 'Code-93'),
        'code93ext': ('Code 93 Extended', 'Code-93-Extended'),
        'interleaved2of5': ('Interleaved 2 of 5', 'Interleaved-2-of-5'),
    },
    'Postal Symbols': {
        'auspost': ('Australia Post', 'Australia-Post'),
        'identcode': ('Deutsche Post Identcode', 'Deutsche-Post-Identcode'),
        'leitcode': ('Deutsche Post Leitcode', 'Deutsche-Post-Leitcode'),
        'japanpost': ('Japan Post', 'Japan-Post'),
        'maxicode': ('MaxiCode', 'MaxiCode'),
        'royalmail': ('Royal Mail', 'Royal-Mail'),
        'kix': ('Royal TNT Post', 'Royal-TNT-Post'),
        'onecode': ('USPS Intelligent Mail', 'USPS-Intelligent-Mail'),
        'postnet': ('USPS POSTNET', 'USPS-POSTNET'),
        'planet': ('USPS PLANET', 'USPS-PLANET'),
        'symbol': ('USPS FIM symbols', 'USPS-FIM-symbols'),
    },
    'Pharmaceutical Symbols': {
        'code32': ('Italian Pharmacode', 'Italian-Pharmacode'),
        'pharmacode': ('Pharmacode', 'Pharmacode'),
        'pharmacode2': ('Two-track Pharmacode', 'Two-track-Pharmacode'),
        'pzn': ('PZN', 'PZN'),
        'hibccode39': ('HIBC Code 39', 'HIBC-Symbols'),
        'hibccode128': ('HIBC Code 128', 'HIBC-Symbols'),
        'hibcpdf417': ('HIBC PDF417', 'HIBC-Symbols'),
        'hibcmicropdf417': ('HIBC MicroPDF417', 'HIBC-Symbols'),
        'hibcqrcode': ('HIBC QR Code', 'HIBC-Symbols'),
        'hibcdatamatrix': ('HIBC Data Matrix', 'HIBC-Symbols'),
        'hibccodablockf': ('HIBC Codablock F', 'HIBC-Symbols'),
    },
    'Less-used Symbols': {
        'bc412': ('BC412', 'BC412'),
        'channelcode': ('Channel Code', 'Channel-Code'),
        'rationalizedCodabar': ('Codabar', 'Codabar'),
        'codablockf': ('Codablock F', 'Codablock-F'),
        'code11': ('Code 11', 'Code-11'),
        'code16k': ('Code 16K', 'Code-16K'),
        'code2of5': ('Code 25', 'Code-25'),
        'dotcode': ('DotCode', 'DotCode'),
        'ultracode': ('Ultracode', 'Ultracode'),
        'iata2of5': ('IATA 2 of 5', 'IATA-2-of-5'),
        'code49': ('Code 49', 'Code-49'),
        'codeone': ('Code One', 'Code-One'),
        'msi': ('MSI Plessey', 'MSI-Plessey'),
        'plessey': ('Plessey', 'Plessey'),
        'posicode': ('PosiCode', 'PosiCode'),
        'telepen': ('Telepen', 'Telepen'),
        'telepennumeric': ('Telepen Numeric', 'Telepen-Numeric'),
    },
    'GS1 Composite Symbols': {
        'ean13composite': ('EAN-13 Composite', 'GS1-Composite-Symbols'),
        'ean8composite': ('EAN-8 Composite', 'GS1-Composite-Symbols'),
        'upcacomposite': ('UPC-A Composite', 'GS1-Composite-Symbols'),
        'upcecomposite': ('UPC-E Composite', 'GS1-Composite-Symbols'),
        'databaromnicomposite': ('GS1 DataBar Omnidirectional Composite', 'GS1-Composite-Symbols'),
        'databarstackedomnicomposite': ('GS1 DataBar Stacked Omnidirectional Composite', 'GS1-Composite-Symbols'),
        'databarexpandedcomposite': ('GS1 DataBar Expanded Composite', 'GS1-Composite-Symbols'),
        'databarexpandedstackedcomposite': ('GS1 DataBar Expanded Stacked Composite', 'GS1-Composite-Symbols'),
        'databartruncatedcomposite': ('GS1 DataBar Truncated Composite', 'GS1-Composite-Symbols'),
        'databarstackedcomposite': ('GS1 DataBar Stacked Composite', 'GS1-Composite-Symbols'),
        'databarlimitedcomposite': ('GS1 DataBar Limited Composite', 'GS1-Composite-Symbols'),
        'gs1-128composite': ('GS1-128 Composite', 'GS1-Composite-Symbols'),
    },
    'Raw Symbols': {
        'daft': ('DAFT', 'DAFT'),
        'flattermarken': ('Flattermarken', 'Flattermarken'),
        'raw': ('Raw', 'Raw'),
    },
    'Partial Symbols': {
        'ean2': ('EAN-2', 'EAN-2'),
        'ean5': ('EAN-5', 'EAN-5'),
        'gs1-cc': ('CC-C', 'GS1-Composite-Symbols#cc-c'),
    },
}
